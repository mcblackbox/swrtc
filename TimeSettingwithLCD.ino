#include <LiquidCrystal.h>
#include <core_build_options.h>
#include <swRTC.h>

LiquidCrystal lcd(12,11,5,4,3,2);        
String lcdString = "";                  

swRTC rtc; 

int switchPin = 9;
int temp;

void Set_AMPM(int hour) {
  if(hour >=12) 
    lcd.print("PM");
  else 
    lcd.print("AM");

  lcd.print(hour%12, DEC);  
}

void Set_lowThanTen(int time) {
  if(time < 10) {
    lcd.print("0");
    lcd.print(time%10);
  }
  else
    lcd.print(time);
}

void setup() {                   
  lcd.begin(16,2);         
  lcd.clear();             
  
  rtc.stopRTC();           
  rtc.setTime(13,33,0);    
  rtc.setDate(26, 12, 2015);  
  rtc.startRTC();         
  
  pinMode(switchPin, INPUT_PULLUP); 
  Serial.begin(9600);      
  Serial.begin(9600);                    
}

void loop() {
  int day;
  lcd.setCursor(0,0);                 
  
  Set_AMPM(rtc.getHours()); 
  lcd.print(":");
  Set_lowThanTen(rtc.getMinutes());
  lcd.print(":");
  Set_lowThanTen(rtc.getSeconds());
  lcd.print("[");
  Set_lowThanTen(rtc.getMonth());
  lcd.print("/");
  Set_lowThanTen(rtc.getDay());
  lcd.print("]");
  lcd.setCursor(0,1);
  lcd.print("6871 CLUB");
  
  
  char theDay[4];
  int i = 0;
  if(Serial.available()) {
    while(Serial.available()) {
      theDay[i] = Serial.read();
      i++;
  }
    day = atoi(theDay);
    if(day/100 >= 12) {
      Serial.print("PM");
      Serial.print((day/100)-12);
    }
    else {
      Serial.print("AM");
      Serial.print(day/100);
    }
    Serial.print(":");
    if(day%100 < 10)
      Serial.print("0");
    Serial.println(day%100);
  }
}
